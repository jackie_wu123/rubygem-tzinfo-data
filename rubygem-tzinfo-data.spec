%global _empty_manifest_terminate_build 0
%global gem_name tzinfo-data
Name:		rubygem-tzinfo-data
Version:	1.2021.1
Release:	1
Summary:	Timezone Data for TZInfo
License:	MIT
URL:		https://tzinfo.github.io
Source0:	https://rubygems.org/gems/tzinfo-data-1.2021.1.gem
BuildArch:	noarch

Requires:	rubygem-tzinfo
BuildRequires:	ruby rsync
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel
Provides:	rubygem-tzinfo-data

%description
TZInfo::Data contains data from the IANA Time Zone database packaged as Ruby modules for use with TZInfo.

%package help
Summary:	Development documents and examples for tzinfo-data
Provides:	rubygem-tzinfo-data-doc
BuildArch: noarch

%description help
TZInfo::Data contains data from the IANA Time Zone database packaged as Ruby modules for use with TZInfo.

%prep
%autosetup -n tzinfo-data-1.2021.1
gem spec %{SOURCE0} -l --ruby > tzinfo-data.gemspec

%build
gem build tzinfo-data.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/%{gem_name}
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
	else
		cp -a .%{gem_extdir_mri}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
pushd %{buildroot}
rm -rf %{buildroot}%{gem_instdir}/.yardopts
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-tzinfo-data -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Mon Aug 02 2021 Ruby_Bot <Ruby_Bot@openeuler.org>
- Package Spec generated
